import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { QuestionComponent } from './question/question.component';
import { HtmlEntityPipe } from './shared/html.entity';
import { PrefsComponent } from './prefs/prefs.component';
import { HttpService } from './shared/http.service';
import { StateService } from './shared/state.service';
import { QuestionsGuard } from './question/question.guard';
import { FeedbackComponent } from './feedback/feedback.component';
import { SummaryComponent } from './summary/summary.component';
import { StatsComponent } from './stats/stats.component';

@NgModule({
  declarations: [
    AppComponent,
    HtmlEntityPipe,
    PrefsComponent,
    QuestionComponent,
    FeedbackComponent,
    SummaryComponent,
    StatsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      { path: '', component: PrefsComponent },
      { path: 'questions', 
        canActivate: [QuestionsGuard],
        component: QuestionComponent },
      { path: 'summary', component: SummaryComponent },
      { path: '', redirectTo: '', pathMatch: 'full' },
      { path: '**', redirectTo: '', pathMatch: 'full' }
    ])
  ],
  providers: [
    HttpService,
    StateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
