import { Injectable } from '@angular/core';


@Injectable()
export class StateService {
    
    questions = [];
    currentQuestionIndex = 0;
    currentQuestionObject = {};
    correctAnswer = '';
    correctAnswerIndex = 0;
    incorrectAnswers = [];
    mergedAnswers = [];
    chosenAnswerStatus = undefined;
    buttonDisabledStatus = false;

    formCategories = [];

    currentTotalCorrect = 0;
    currentScore = 0;


    config = {
        amount: 10,
        category: 'any',
        difficulty: 'any',
        type: 'any',
        finalUrl: ''
    }
    
    isLoaded = 0;
    formSubmitted = 0;
}





