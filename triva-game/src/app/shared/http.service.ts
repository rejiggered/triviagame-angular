import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';


@Injectable()
export class HttpService {
    constructor(private http: HttpClient) {}

    getData(url): Observable<any> {
        console.log(url);
        return this.http.get(url)
            .pipe(
                tap(data => console.log(data)),
                catchError(this.handleError)
        );
    }

    private handleError(err: HttpErrorResponse) {
        let errorMessage = '';
        console.error('ErrorMessage');
        return throwError(errorMessage);
    }
}    