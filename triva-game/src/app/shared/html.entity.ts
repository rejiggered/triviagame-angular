import { Pipe, PipeTransform } from '@angular/core'

@Pipe({
    name: 'htmlEntity'
})

export class HtmlEntityPipe implements PipeTransform {
    transform(value: any, args: any[]): any {
        if (!value) return;
        let txt = document.createElement("textarea");
        txt.innerHTML = value;
        return txt.value;
    }
} 