import { Component, OnInit } from '@angular/core';

import { StateService } from '../shared/state.service';

@Component({
  selector: 'tr-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  constructor(
    public state: StateService
  ) { }


    // Calculate the current score and save to state object
    calculateScore() {
      var currentState = this.state.chosenAnswerStatus;
      if (currentState === 1) {
          this.state.currentTotalCorrect += 1;
      }
      this.state.currentScore = Math.round(((this.state.currentTotalCorrect / (this.state.currentQuestionIndex + 1)) * 100));
    }


  ngOnInit() {
  }

}
