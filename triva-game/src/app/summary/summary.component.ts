import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { StateService } from '../shared/state.service';

@Component({
  selector: 'tr-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent {

  constructor(
    public state: StateService,
    private router: Router
  ) {}

  
  navigateToQuestionsView() {
    this.router.navigate(['/questions'], {skipLocationChange:true});
  }

  navigateToPrefsView() {
    this.router.navigate([''], {skipLocationChange:true});
  }



}
