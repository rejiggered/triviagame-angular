import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { StateService } from '../shared/state.service';

@Component({
  selector: 'tr-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss']
})
export class FeedbackComponent implements OnInit {

  @Output()
  nextQuestionClick = new EventEmitter<String>(); //creating an output event

  constructor( 
    public state: StateService,
    private router: Router,
  ) { }


  // Negative Comments for wrong answers
  negativeResponses = [
    'Oooh, bummmer. The correct answer is',
    'Man, you stink. The correct answer is',
    'Gosh, you\'re not very good, are you? The correct answer is',
    'Sorry amigo, that\s not it. The correct answer is',
    'Nope, wrong. The correct answer is'
  ];

  // Positive Comments for right answers
  positiveResponses = [
    'Nice job! The correct answer is',
    'Woo hoo, you\'re doing great! The correct answer is',
    'Sweet, you got it! The correct answer is',
    'Way to go! The correct answer is',
    'Not too shabby! That\'s right, the correct answer is'
  ];

  feedbackResponse: string = "";


  // randomize the feedback responses
  assembleFeedbackResponse() {
    if(this.state.chosenAnswerStatus !==1) {
      this.feedbackResponse = this.negativeResponses[Math.floor(Math.random() * this.negativeResponses.length)];
    } else {
      this.feedbackResponse = this.positiveResponses[Math.floor(Math.random() * this.positiveResponses.length)];
    }
  }

  // Determine if there are additional questions to display, otherwise forward to Summary view
  advanceNextQuestion(number) {
    if((this.state.currentQuestionIndex + 2) > this.state.config.amount) {
      this.navigateToSummaryView()
    } else {
      this.changePagingIndex(number);
    }
  }

  // Paging logic
  changePagingIndex(number): void {
    if  (this.state.currentQuestionIndex > 0 && number < 0 ||  //index must be greater than 0 at all times
        this.state.currentQuestionIndex < (this.state.questions.length -1) && number > 0 ) {  //index must be less than length of array
        this.state.currentQuestionIndex += number;
    }
    this.state.currentQuestionObject = this.state.questions[this.state.currentQuestionIndex];
    this.state.correctAnswer = this.state.questions[this.state.currentQuestionIndex].correct_answer;
    this.state.incorrectAnswers = this.state.questions[this.state.currentQuestionIndex].incorrect_answers;

    // set state back to undefined
    this.state.chosenAnswerStatus = undefined;

    // emit event for parent Question Component to fire
    this.nextQuestionClick.emit();
  }


  navigateToSummaryView() {
    this.router.navigate(['/summary'], {skipLocationChange:true});
  }

  ngOnInit() {
  }

}
