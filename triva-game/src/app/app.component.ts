import { Component } from '@angular/core';
import { fadeAnimation } from './shared/fade.animation';

@Component({
  selector: 'tr-root',
  template:
    `<main class="container" [@fadeAnimation]="o.isActivated ? o.activatedRoute : ''">
      <router-outlet #o="outlet"></router-outlet>
    </main>`,
  styleUrls: ['./app.component.scss'],
  animations: [fadeAnimation]
})
export class AppComponent {
}
