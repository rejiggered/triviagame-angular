import { Component, OnInit, ViewChild } from '@angular/core';

import { StateService } from '../shared/state.service';
import { HttpService } from '../shared/http.service';
import { FeedbackComponent } from '../feedback/feedback.component';
import { StatsComponent } from '../stats/stats.component';

@Component({
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
    
  errorMessage: string;

  constructor(
    private httpService: HttpService,
    public state: StateService
  ) {}

  // Allow access to Feedback component to call responses
  @ViewChild(FeedbackComponent) childFeedback: FeedbackComponent;
  @ViewChild(StatsComponent) childStats: StatsComponent;



  // Call HTTP Service and get the quesion data
  getQuestions() {
    this.httpService.getData(this.state.config.finalUrl).subscribe(
      questions => { 
        this.state.questions = questions.results;
        this.setInitialValues();

        error => this.errorMessage = <any>error;
        this.state.isLoaded = 1;
      });
  }

  // Set base State values
  setInitialValues(): void {
    this.state.currentQuestionObject = this.state.questions[this.state.currentQuestionIndex];
    this.state.correctAnswer = this.state.questions[this.state.currentQuestionIndex].correct_answer;
    this.state.incorrectAnswers = this.state.questions[this.state.currentQuestionIndex].incorrect_answers;
    
    this.determineQuestionType();
  }


  getRand() {
    return Math.floor(Math.random() * (4 - 0)) + 0;
  }


  // Check to see if multiple choice or true/false question
  determineQuestionType() {
    if (this.state.questions[this.state.currentQuestionIndex].type == 'multiple') {
      this.mergeMultipeChoiceAnswers();
    } else {
      this.mergeTrueFalseAnswers();
    }
  }

  // Randomly merge the Incorrect Answer array with the Correct Answer and it's index
  mergeMultipeChoiceAnswers(): void {
    this.state.mergedAnswers = this.state.incorrectAnswers.slice();
    this.state.correctAnswerIndex = this.getRand();
    this.state.mergedAnswers.splice(this.state.correctAnswerIndex, 0, this.state.correctAnswer);
  }

  // Combine T/F Answers and put in correct order
  mergeTrueFalseAnswers() {
    if (this.state.correctAnswer == "True") {
      this.state.mergedAnswers = [this.state.correctAnswer, this.state.incorrectAnswers];
      this.state.correctAnswerIndex = 0;
    } else {
      this.state.mergedAnswers = [this.state.incorrectAnswers, this.state.correctAnswer];
      this.state.correctAnswerIndex = 1;
    }
  }

  // Process the selected answer on click
  processAnswer(event)  {
    if( parseInt(event.srcElement.value) !== this.state.correctAnswerIndex ) {
      this.state.chosenAnswerStatus = 0;
    } else {
      this.state.chosenAnswerStatus = 1;
    }
    // Call function in child Feedback component
    this.childFeedback.assembleFeedbackResponse();

    // Call function to calculate the current score
    this.childStats.calculateScore();
  }



  // Reset state
  resetGameState() {
    this.state.currentQuestionIndex = 0;
    this.state.currentTotalCorrect = 0;
    this.state.currentScore = 0;
    this.state.formSubmitted = 0;
    this.state.chosenAnswerStatus = undefined;
    this.state.questions = [];
    this.state.currentQuestionObject = {};
  }




  ngOnInit(): void {
    this.resetGameState();
    this.getQuestions();
  }

}
  