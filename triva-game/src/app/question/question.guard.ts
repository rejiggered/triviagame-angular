import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { StateService } from '../shared/state.service';


@Injectable({
    providedIn: 'root'
})
export class QuestionsGuard implements CanActivate {

    constructor(
        private router: Router,
        private state: StateService
    ) {}

    private test = this.state.formSubmitted;

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
            
            // if (this.test == 0) {
            //     this.router.navigate(['']);
            //     return false;
            // };
            
            return true;   
            
        } 
        
} 
    



