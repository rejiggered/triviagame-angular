import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { StateService } from '../shared/state.service';
import { HttpService } from '../shared/http.service';

@Component({
  templateUrl: './prefs.component.html',
  styleUrls: ['./prefs.component.scss']
})
export class PrefsComponent implements OnInit {

  errorMessage: string;
  baseURL:string = 'https://opentdb.com/api.php';

  constructor(
    private httpService: HttpService,
    private router: Router,
    public state: StateService
  ) {}



  private getCategories() {
    this.httpService.getData('https://opentdb.com/api_category.php').subscribe(
      categories => { 
        this.state.formCategories = categories.trivia_categories;
        console.log(this.state.formCategories)
        error => this.errorMessage = <any>error;
      });     
  }

  // Retrieve form values
  private getFormData(formValues) {
    this.state.config.amount = formValues.questionAmount;
    this.state.config.category = formValues.category;
    this.state.config.difficulty = formValues.difficulty;
    this.state.config.type = formValues.type;

    console.log(formValues);


    this.assembleQuery();

    this.state.formSubmitted = 1;
    this.navigateToQuestionsView();
   
  }

  // Assemble the selected Prefs object
  private assembleQuery() { 
    var options = {};
    for (var key in this.state.config) {
      // Don't include the category name
      if (key != 'finalUrl') {
        if (this.state.config[key] != 'any') {
            options[key] = this.state.config[key];
        }
      }
    }
    // Assign the final concatenated query fragment
    var fragment = this.buildQueryString(options);

    // The final assembled URL query string
    var url = this.baseURL + fragment;
    
    // Update state config with final assembled URL
    this.state.config.finalUrl = url;
}

  // Utility to build the actual query string
  private buildQueryString(parms) {
    var parts = [];
    for (var key in parms) {
        var str = key + '=' + parms[key];
        parts.push(str);
    }
    var result = parts.join('&');
    if (parts.length > 0) {
        result = '?' + result;
    }
    return result;
  }


  navigateToQuestionsView() {
    this.router.navigate(['/questions'], {skipLocationChange:true});
  }
  


  ngOnInit() {
    this.getCategories();
  }
}
